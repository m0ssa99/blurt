#pragma once

#include <vector>

namespace blurt { namespace plugins { namespace p2p {

#ifdef IS_TEST_NET
const std::vector< std::string > default_seeds;
#else
const std::vector< std::string > default_seeds = {
   "78.46.137.254:2001",   // seednode1
   "88.198.76.136:2001",   // seednode2
   "65.21.190.11:1776",    // rpc-1
   "65.21.179.209:1776",   // rpc-2
   "65.21.181.160:1776",   // rpc-3
   "65.21.189.82:1776",    // rpc-4
   "194.163.159.164:1776", // blurtlatam
   "207.244.233.24:1776",  // opfergnome aka nerdtopiade
   "209.126.86.39:1776",   // michelangelo3
   "blurt-seed1.saboin.com:1776", // saboin
   "blurt-seed2.saboin.com:1776"  // saboin
};
#endif

} } } // blurt::plugins::p2p
