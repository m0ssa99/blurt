const config = require("config.json")("./config/config.json");
const express = require("express");
const multer = require("multer");
const createError = require('http-errors');
const cors = require('cors');
const chainLib = require("@blurtfoundation/blurtjs");
const { createHash } = require("crypto");
const {
  PublicKey,
  Signature,
} = require("@blurtfoundation/blurtjs/lib/auth/ecc");
const RIPEMD160 = require("ripemd160");
const AWS = require("aws-sdk");
const { RateLimiterMemory } = require("rate-limiter-flexible");

chainLib.api.setOptions({
  url: config.blurt_rpc_node,
  alternative_api_endpoints: config.alternative_blurt_rpc_nodes,
  retry: true,
  useAppbaseApi: true,
});

const acceptedMimeTypes = [
  'image/gif',
  'image/jpeg',
  'image/png',
  'image/webp'
]

const uploadFileFilter = (req, file, cb) => {

  if (acceptedMimeTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(createError(400, "Invalid file type; allowed types are: gif, jpeg, png, webp."), false);
  }
}

const storage = multer.memoryStorage()

const upload = multer({
  storage: storage,
  fileFilter: uploadFileFilter,
  limits: {
    files: 1,
    fileSize: config.max_image_size_in_bytes,
  }
});

const s3 = new AWS.S3({
  accessKeyId: config.s3_config.access_key_id,
  secretAccessKey: config.s3_config.secret_access_key,
  region: config.s3_config.region,
  endpoint: config.s3_config.endpoint,
  signatureVersion: "v4",
});

const rate_limit_opts = {
  points: config.rate_limit_points, // number of images
  duration: 3600, // per hour
};
const rateLimiter = new RateLimiterMemory(rate_limit_opts);

const rateLimiterMiddleware = (req, res, next) => {
  rateLimiter.consume(req.params["username"])
    .then((rateLimiterRes) => {
      res.set('X-RateLimit-Remaining', rateLimiterRes.remainingPoints)
      res.set("X-RateLimit-Reset", new Date(Date.now() + rateLimiterRes.msBeforeNext))
      next();
    })
    .catch((rateLimiterRes) => {
      res.set('X-RateLimit-Remaining', rateLimiterRes.remainingPoints)
      res.set("X-RateLimit-Reset", new Date(Date.now() + rateLimiterRes.msBeforeNext))
      next(createError(429, `Upload quota exceded; You're allowed ${rate_limit_opts.points} images per hour.`));
    });
};

const app = express();
const port = config.port || 7000; // set our port

hdl_upload_s3 = async (req, res, next) => {

  try {
    const { username, sig } = req.params;
    if (username === undefined || username === null) {
      throw createError(404, "User not found.");
    }

    const content_type = req.file.mimetype;
    const split_file_name = req.file.originalname.split(".");
    const file_ext = split_file_name[split_file_name.length - 1];
    const hash_buffer = new RIPEMD160().update(req.file.buffer).digest("hex");
    const s3_file_path = `${username}/${hash_buffer}.${file_ext}`;

    // verifying sig
    const isNotValidUsername = chainLib.utils.validateAccountName(username);
    if (isNotValidUsername) {
      throw createError(400, "Invalid username.");
    }

    const existingAccs = await chainLib.api.getAccountsAsync([username]);
    if (existingAccs.length !== 1) {
      throw createError(404, "User does not exist.");
    }

    let sign_data = Signature.fromBuffer(new Buffer.from(sig, "hex"));

    const imageHash = createHash('sha256')
      .update('ImageSigningChallenge')
      .update(req.file.buffer)
      .digest();

    const sigPubKey = sign_data.recoverPublicKey(imageHash).toString();

    const postingPubKey = existingAccs[0].posting.key_auths[0][0];
    const activePubKey = existingAccs[0].active.key_auths[0][0];
    const ownerPubKey = existingAccs[0].owner.key_auths[0][0];

    switch (sigPubKey) {
      case postingPubKey:
      case activePubKey:
      case ownerPubKey:
        // key matched, do nothing
        break;
      default:
        throw createError(400, "Invalid signing key.");
    }

    const is_verified = sign_data.verifyHash(
      imageHash,
      PublicKey.fromString(sigPubKey)
    );
    if (!is_verified) {
      throw createError(400, "Invalid signature.");
    }

    await s3
      .putObject({
        ACL: "public-read",
        Bucket: config.s3_config.bucket,
        Key: s3_file_path,
        Body: req.file.buffer,
        ContentType: content_type,
      })
      .promise();

    const img_full_path = `${config.prefix_url}${config.s3_config.bucket}/${s3_file_path}`;
    res.status(200).json({ status: "ok", message: "Successfully uploaded image.", url: img_full_path });
  } catch (e) {
    next(e);
  }
};

serverStart = () => {
  app.use(cors())
  const router = express.Router();
  router.post("/:username/:sig", rateLimiterMiddleware, upload.single("file"), hdl_upload_s3);
  router.get("/test_cors", async (req, res) => {
    console.log("Got a request on test_cors endpoint.")
    res.json({ status: "ok", message: "success", data: null });
  });

  app.use("/", router);

  app.listen(port);
  console.log(`serverStart on port ${port}`);
};

serverStart();

module.exports = app;
